const webpack = require('webpack');
const slsw = require('serverless-webpack')

const plugins = []
plugins.push(new webpack.NormalModuleReplacementPlugin(/\/iconv-loader$/, 'node-noop'))

module.exports = {
  entry:  slsw.lib.entries,
  target: 'node',
  plugins: plugins,
  module: {
    loaders: [{
        test: /\.js|\.es6$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        query: {
          presets: ['env']
        }
      },
      {
        test: /\.json$/,
        loader: 'json-loader'
      }
    ]
  }
}
