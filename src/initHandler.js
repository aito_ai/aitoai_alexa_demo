'use strict'

const Alexa = require('alexa-sdk')
const helper = require('./helpers')

const initHandler = Object.assign({}, defaultHandlers, {
  'NewSession': function () {
    // Reset the session here.
  },
  'NameCharactersIntent': function () {
    // Change state to the characters FSM
    this.handler.state = "_CHARACTERS"
    this.emitWithState('RandomCharacterIntent')
  },
  'StartSessionIntent': function () {
    this.emit(':ask',
     // The default prompt
     'Welcome to film trivia. I can quiz you on movie characters, ' +
     'or you could try to see whether you know dialogue from movies.' +
     'Would you like to try the dialogue or the characters?',
     // Reprompt, if we did not catch what the user said
     'Choose dialogue or characters to continue')
  },
  'MovieLinesIntent': function () {
    // Chnage state to the dialogue state
    this.handler.state = "_DIALOGUE"
    this.emitWithState('RandomPhraseIntent')
  },
  'Amazon.HelpIntent': function () {
    console.log("Asked for help")
    this.emit(':ask',"No help available. Sorry!")
  },
  'Unhandled': function() {
    console.log("Unhandled event in initHandler")
    let resp = helper.unhandledResponse()
    this.emit(':ask', resp)
  }
})


const exitGame = function () {
  this.emit(':tell', 'Goodbye!')
}

const defaultHandlers = {
  'AMAZON.StopIntent': exitGame,
  'AMAZON.CancelIntent': exitGame,
  'AMAZON.NoIntent': exitGame
}


module.exports = initHandler
