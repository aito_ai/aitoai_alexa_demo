'use strict'

const Alexa = require('alexa-sdk')
//const helper = require('./helpers')

const exitGame = function () {
  this.emit(':tell', 'Goodbye!')
}

const defaultHandlers = {
  'AMAZON.StopIntent': exitGame,
  'AMAZON.CancelIntent': exitGame,
  //'AMAZON.NoIntent': exitGame,
  'SessionEndedRequest': exitGame,
  'Unhandled': function() {
    let resp = unhandledResponse[Math.floor(Math.random() * unhandledResponse.length)]
    console.log("Unhandled event")
    this.response.speak(resp)
    this.emit(':responseReady');
  }
}

const unhandledResponse = Array(
  "Sorry, couldn't quite catch that",
  "Come again?",
  "Could you please repeat. I'm having trouble hearing you.",
  "Again, please",
  "Excuse me?"
)

const excuseMe = () => {
  unhandledResponse[Math.floor(Math.random() * unhandledResponse.length)]
}


const dialogueHandler = Alexa.CreateStateHandler("_DIALOGUE",
  Object.assign({}, defaultHandlers, {
    'Amazon.HelpIntent': function() {
      console.log("Asked for help")
      this.emit(':ask', "No help available. Sorry!")
    },
    'RandomPhraseIntent': function() {
      this.emit(':ask', 'Choosing random phrase from the database.',"What comes next?")
    },
    'CatchAllIntent': function() {
      const phrase = this.event.request.intent.slots.phrase.value
      console.log(`CatchAllIntent ${phrase}`)
      this.emit(':ask', `You said: ${phrase}`, "Come again?")
    },
    'Unhandled': function() {
      console.log("Unhandled event in dialogueHandler")
      let resp = excuseMe()
      this.emit(':ask', resp)
    }
  })
)

module.exports = dialogueHandler
