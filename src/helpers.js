'use strict'

const exitGame = function () {
  this.emit(':tell', 'Goodbye!')
}

const defaultHandlers = {
  'AMAZON.StopIntent': exitGame,
  'AMAZON.CancelIntent': exitGame,
  //'AMAZON.NoIntent': exitGame,
  'SessionEndedRequest': exitGame,
  'Unhandled': function() {
    let resp = unhandledResponse[Math.floor(Math.random() * unhandledResponse.length)]
    console.log("Unhandled event")
    this.response.speak(resp)
    this.emit(':responseReady');
  }
}

const unhandledResponse = Array(
  "Sorry, couldn't quite catch that",
  "Come again?",
  "Could you please repeat. I'm having trouble hearing you.",
  "Again, please",
  "Excuse me?"
)

const excuseMe = () => {
  unhandledResponse[Math.floor(Math.random() * unhandledResponse.length)]
}


module.exports = {
  'defaults': defaultHandlers,
  'unhandledResponse': excuseMe
}
