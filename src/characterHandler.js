'use strict'

const Alexa = require('alexa-sdk')

const exitGame = function () {
  this.emit(':tell', 'Goodbye!')
}

const defaultHandlers = {
  'AMAZON.StopIntent': exitGame,
  'AMAZON.CancelIntent': exitGame,
  'Unhandled': function() {
    let resp = unhandledResponse[Math.floor(Math.random() * unhandledResponse.length)]
    this.emit(':ask', resp)
  }
}

const unhandledResponse = Array(
  "Sorry, couldn't quite catch that",
  "Come again?",
  "Could you please repeat. I'm having trouble hearing you.",
  "Again, please",
  "Excuse me?"
)

const excuseMe = () => {
  unhandledResponse[Math.floor(Math.random() * unhandledResponse.length)]
}

const characterHandler = Alexa.CreateStateHandler("_CHARACTERS",
  Object.assign({}, defaultHandlers, {
    'Amazon.HelpIntent': function () {
      console.log("Asked for help")
      this.emit(':ask', "No help available. Sorry!")
    },
    'RandomCharacterIntent': function () {
      this.emit(':ask', "Getting random phrase from the db. Can you tell me who said it, and in which movie?", "Do you know the character and the movie?")
    },
    'GuessCharacterIntent': function () {
      const movie = this.event.request.intent.slots.movie.value
      const character = this.event.request.intent.slots.character.value
      console.log(`CatchAllIntent M:${movie} and c:${character}`)
      this.emit(':ask', `Your guess is movie: ${movie} and character ${character} is either correct or not`, "Please guess again")
    },
    'Unhandled': function() {
      console.log("Unhandled event in characterHander. Getting random response")
      let resp = excuseMe()
      this.emit(':ask', resp)
    }
  })
)


module.exports = characterHandler
