# Example project integrating the Aito.ai Intelligent Database and Amazon Alexa
Example project for
a) trying out Amazon Alexa
b) providing some real life example cases for Aito.ai

This project is provided as an example, and is not built for production use. Please feel free
to clone and play with it. Pull-requests are also welcome.

# License
This project is distributed under the Apache License, Version 2.0, see LICENSE.txt and NOTICE.txt for more information.
