'use strict';
const Alexa = require('alexa-sdk')

const initHandler = require('./src/initHandler')
const dialogueHandler = require('./src/dialogueHandler')
const characterHandler = require('./src/characterHandler')
const settings = require('./environment.json')

module.exports.alexa = (event, context, callback) => {
  var alexa = Alexa.handler(event, context, callback)
  alexa.appId = settings.skillId
  // alexa.dynamoDBTableName = 'speech-store-state?';
  alexa.registerHandlers(initHandler, dialogueHandler, characterHandler)
  alexa.execute()
}
